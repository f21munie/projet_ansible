# Description du projet

- Auteur : Florian MUNIER
- Contact : florian.munier@imt-atlantique.net
- Formation : MS Infrastructures Cloud et DevOps
- Date : 16/01/2023 - 26/01/2023
- Description : Ce projet a pour but de déployer l'application Vapormap (https://gitlab.imt-atlantique.fr/vapormap/vapormap-app.git) sur un host distant via Ansible.

# Clone du projet

```sh
git clone https://gitlab.imt-atlantique.fr/f21munie/projet_ansible.git
```

# Création d'un environnement virtuel

```sh
python3 -m venv ./venv/ansible
source ./venv/ansible/bin/activate
pip install ansible ansible-core ansible-lint yamllint
```

# Modification de l'hôte distant

Veuillez à bien renseigner <HOST_IP> par votre IP d'hôte et <SSH_KEY> par le nom de votre clé SSH.

```sh
cd projet_ansible/
nano host.ini
vapormap	ansible_host=<HOST_IP>	ansible_user=ubuntu	ansible_ssh_private_key_file=~/.ssh/<SSH_KEY>
```

# Lancement du playbook deploy (activer venv)

```sh
cd projet_ansible
ansible-playbook -i host.ini deploy.yml
```

# Lancement du playbook destroy (activer venv)

```sh
cd projet_ansible
ansible-playbook -i host.ini destroy.yml
```
